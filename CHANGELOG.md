# F4W Rack middlewares #

## [Unreleased] ##

## [0.2.0] - 2016-12-30 ##
Added
- testing & linting
- dry-initiliazer
- OAuth2 mock middleware for testing purpose

## 0.1.0 - 2016-11-15 ##

### Added ###
- OAuth2 checking middleware
- Logging middleware


[Unreleased]: https://gitlab.com/unifi-f4w/f4w-rack/compare/0.1.0...develop
[0.2.0]: https://gitlab.com/unifi-f4w/f4w-rack/compare/0.1.0...0.2.0
