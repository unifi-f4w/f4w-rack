# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'f4w/rack/version'

Gem::Specification.new do |spec|
  spec.name          = 'f4w-rack'
  spec.version       = F4w::Rack::VERSION
  spec.authors       = ['Alessio Caiazza']
  spec.email         = ['nolith@abisso.org']

  spec.summary       = 'A collection of rack middleware'
  spec.description   = 'A collection of rack middleware related to FACTS4WORKERS EU project'
  spec.homepage      = 'https://gitlab.com/unifi-f4w/f4w-rack'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'rack', '>= 1.6.4'
  spec.add_dependency 'oauth2-checker', '~> 0.2.0'
  spec.add_dependency 'dry-initializer', '~> 0.10.0'

  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'bundler', '~> 1.13'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_development_dependency 'minitest-rg', '~> 5.2.0'
  spec.add_development_dependency 'simplecov', '~> 0.12.0'
  spec.add_development_dependency 'rubocop', '~> 0.46.0'
end
