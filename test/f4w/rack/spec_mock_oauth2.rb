require 'test_helper'
require 'rack/lint'
require 'rack/mock'
require 'f4w/rack/mock_oauth2'

describe F4w::Rack::MockOauth2 do
  def unprotected_app
    Rack::Lint.new lambda { |env|
      if env['rack.oauth2.valid_token']
        [200, { 'Content-Type' => 'text/plain' },
         ["Hi #{env['rack.oauth2.resource_owner']}"]]
      else
        [403, { 'Content-Type' => 'text/plain' }, ['invalid token']]
      end
    }
  end

  def request(token = '')
    yield @request.get("/?access_token=#{token}")
  end

  describe 'when using default token_validator' do
    def protected_app
      F4w::Rack::MockOauth2.new(unprotected_app)
    end

    before do
      @request = Rack::MockRequest.new(protected_app)
    end

    it 'return validates every token' do
      request('Test') do |response|
        response.status.must_equal 200
        response.body.to_s.must_equal 'Hi mock_owner'
      end

      request('reagds232') do |response|
        response.status.must_equal 200
        response.body.to_s.must_equal 'Hi mock_owner'
      end
    end

    it 'returns 403 without token' do
      response = @request.get('/')
      response.status.must_equal 403
      response.body.to_s.must_equal 'invalid token'
    end
  end

  describe 'when a custom validator is provided' do
    def protected_app
      F4w::Rack::MockOauth2.new(unprotected_app, @token_validator)
    end

    before do
      @token_validator = lambda do |token|
        valid = !token.nil? && token[0] == 'a'
        if valid
          [true, "owner_#{token}"]
        else
          [false, nil]
        end
      end
      @request = Rack::MockRequest.new(protected_app)
    end

    it 'returns 403 without token' do
      response = @request.get('/')
      response.status.must_equal 403
      response.body.to_s.must_equal 'invalid token'
    end

    it 'returns 403 with an invalid token' do
      ('b'..'z').map { |i| "#{i}_token" }.each do |token|
        request(token) do |response|
          response.status.must_equal 403, "Testing token #{token}"
          response.body.to_s.must_equal 'invalid token'
        end
      end
    end

    it 'returns 200 with a valid token' do
      ('b'..'z').map { |i| "a_#{i}_token" }.each do |token|
        request(token) do |response|
          response.status.must_equal 200, "Testing token #{token}"
        end
      end
    end

    it 'uses the right parameters' do
      app = protected_app
      app.token_validator.must_equal @token_validator
    end
  end
end
