require 'test_helper'
require 'rack/lint'
require 'rack/mock'

describe F4w::Rack::Oauth2 do
  def client_id
    'client_id'
  end

  def client_secret
    'client_secret'
  end

  def options
    { site: 'https://example.com/oauth/' }
  end

  def unprotected_app
    Rack::Lint.new lambda { |env|
      if env['rack.oauth2.valid_token']
        [200, { 'Content-Type' => 'text/plain' },
         ["Hi #{env['rack.oauth2.resource_owner']}"]]
      else
        [403, { 'Content-Type' => 'text/plain' }, ['invalid token']]
      end
    }
  end

  def protected_app
    F4w::Rack::Oauth2.new(unprotected_app, client_id, client_secret, options)
  end

  before do
    @request = Rack::MockRequest.new(protected_app)
  end

  def request(token = '')
    yield @request.get("/?access_token=#{token}")
  end

  it 'return application output if correct credentials are specified' do
    @checker = Minitest::Mock.new
    @checker.expect :validate, [true, 'Test'], ['Test']

    ::OAuth2::Checker.stub :new, @checker do
      request('Test') do |response|
        response.status.must_equal 200
        response.body.to_s.must_equal 'Hi Test'
      end
    end

    @checker.verify
  end

  it 'returns 403 without token' do
    @checker = Minitest::Mock.new
    @checker.expect :validate, [false, nil], ['']

    ::OAuth2::Checker.stub :new, @checker do
      request do |response|
        response.status.must_equal 403
        response.body.to_s.must_equal 'invalid token'
      end
    end

    @checker.verify
  end

  it 'uses the right parameters' do
    app = protected_app
    checker = app.send('oauth2_checker')
    checker.client.id.must_equal client_id
    app.options.must_equal options
  end
end
