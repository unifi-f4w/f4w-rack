require 'test_helper'

module F4w
  class RackTest < Minitest::Test
    def test_that_it_has_a_version_number
      refute_nil ::F4w::Rack::VERSION
    end
  end
end
