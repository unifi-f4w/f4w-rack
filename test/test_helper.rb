require 'simplecov'
SimpleCov.start
$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'f4w/rack/all'

require 'minitest/rg'
require 'minitest/autorun'
