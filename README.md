# F4w::Rack

This gems offers some rack middlewares specifically developed for FACTS4WORKERS.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'f4w-rack'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install f4w-rack

