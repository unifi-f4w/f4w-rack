module F4w
  module Logging
    class AppData
      attr_accessor :data

      def initialize(data = {})
        @data = data
      end

      def as_json(_options = nil)
        { 'appData' => @data.to_json }
      end
    end
  end
end
