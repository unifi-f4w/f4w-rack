require 'date'
require 'f4w/logging/app_data'
require 'f4w/logging/error_data'

module F4w
  module Logging
    class Entry
      VERSION = 1

      attr_accessor :app_data_log_record
      attr_accessor :error_log_record
      attr_accessor :invocation_zoned_date_time
      attr_accessor :service_uri
      attr_accessor :type_of_call
      attr_accessor :user_id

      def initialize(uri, type_of_call, user_id, app_data, error_data)
        @service_uri = uri
        @type_of_call = type_of_call
        @app_data_log_record = app_data
        @error_log_record = error_data
        @user_id = user_id

        @invocation_zoned_date_time = DateTime.now.iso8601(3)
      end

      def as_json(options = nil)
        json = {
          'invocationZonedDateTime' => @invocation_zoned_date_time,
          'serviceURI' => @service_uri,
          'tyepeOfCall' => @type_of_call
        }

        json['appDataLogRecord'] = @app_data_log_record.as_json(options) unless @app_data_log_record.nil?
        json['errorLogRecord'] = @error_log_record.as_json(options) unless @error_log_record.nil?
        json['userID'] = @user_id unless @user_id.nil?

        json
      end

      def self.request_start(uri, user_id, data = nil)
        log_request(uri, '1', user_id, data)
      end

      def self.request_end(uri, user_id, data = nil)
        log_request(uri, '2', user_id, data)
      end

      def self.error(uri, user_id, error_context, error_message, data = nil)
        log_request(uri, '-1', user_id, data, error_context, error_message)
      end

      def to_s
        "ver=#{VERSION} log=#{to_json}"
      end

      private

      def self.log_request(uri, type, user_id, data = nil, error_context = nil, error_message = nil)
        error = if !error_context.nil? && !error_message.nil?
                  ErrorData.new(error_context, error_message)
                end
        new(uri, type, user_id, data.nil? ? nil : AppData.new(data), error)
      end
    end
  end
end
