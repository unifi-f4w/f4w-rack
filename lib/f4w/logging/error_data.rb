module F4w
  module Logging
    class ErrorData < Struct.new(:context, :message)
    end
  end
end
