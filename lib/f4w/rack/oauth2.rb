require 'dry-initializer'
require 'rack/request'
require 'oauth2/checker'

module F4w
  module Rack
    # Validates oauth token
    class Oauth2
      include Dry::Initializer.define -> do # rubocop:disable Style/Lambda
        param :app
        param :client_id
        param :client_secret
        param :options
      end

      def call(env)
        req = ::Rack::Request.new(env)
        if req.params['access_token']
          validate_token(req, env)
        else
          env['rack.oauth2.valid_token'] = false
        end

        @app.call(env)
      end

      private

      def oauth2_checker
        ::OAuth2::Checker.new(@client_id, @client_secret, @options)
      end

      def validate_token(request, env)
        logger = request.logger || ::Logger.new($stdout)

        logger.debug('getting Oauth2 resource_owner')
        valid, resource_owner = oauth2_checker
                                .validate(request.params['access_token'])
        env['rack.oauth2.valid_token'] = valid
        env['rack.oauth2.resource_owner'] = resource_owner
        logger.debug("resource_owner: #{resource_owner}")
      end
    end
  end
end
