require 'rack/request'
require 'f4w/logging/entry'

module F4w
  module Rack
    class Logger
      def initialize(app)
        @app = app
      end

      def call(env)
        req = ::Rack::Request.new(env)
        requested_uri = req.url
        user_id = env['rack.oauth2.resource_owner']
        data = app_data(env)
        puts F4w::Logging::Entry.request_start(requested_uri, user_id, data)
        begin
          status, headers, body = @app.call(env)

          [status, headers, body]
        rescue Exception => e
          puts F4w::Logging::Entry.error(requested_uri, user_id, 'uncatched_exception', e.message, data.merge(error_class: e.class))
          throw e
        ensure
          puts F4w::Logging::Entry.request_end(requested_uri, user_id, data)
        end
      end

      private

      def app_data(env)
        env.dup.delete_if { |key, _| !(key =~ /^HTTP_/) || key == 'HTTP_COOKIE' }
      end
    end
  end
end
