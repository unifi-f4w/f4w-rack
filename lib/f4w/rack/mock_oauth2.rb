require 'dry-initializer'
require 'rack/request'

module F4w
  module Rack
    # A Mock that behaves like <tt>F4w::Rack::Oauth2</tt>
    class MockOauth2
      include Dry::Initializer.define -> do # rubocop:disable Style/Lambda
        param :app
        param :token_validator,
              default: proc { ->(_token) { [true, 'mock_owner'] } }
      end

      def call(env)
        req = ::Rack::Request.new(env)
        if req.params['access_token']
          validate_token(req, env)
        else
          env['rack.oauth2.valid_token'] = false
        end

        @app.call(env)
      end

      private

      def validate_token(request, env)
        valid, resource_owner = @token_validator
                                .call(request.params['access_token'])
        env['rack.oauth2.valid_token'] = valid
        env['rack.oauth2.resource_owner'] = resource_owner
      end
    end
  end
end
